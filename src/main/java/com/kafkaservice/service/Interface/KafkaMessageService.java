package com.kafkaservice.service.Interface;

import java.util.Map;

public interface KafkaMessageService {
    String processMessage(Map<String, Object> messageBody);
}
