package com.kafkaservice.service;

import com.kafkaservice.kafka.KafkaProducer;
import com.kafkaservice.service.Interface.KafkaMessageService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class KafkaMessageServiceImpl implements KafkaMessageService {

    @Autowired
    KafkaProducer kafkaProducer;

    @Override
    public String processMessage(Map<String, Object> messageBody) {
        String message = messageBody.get("message").toString();
        kafkaProducer.sendKafkaTopic(message);
        return "Message processed";
    }
}